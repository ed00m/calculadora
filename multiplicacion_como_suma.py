# -*- coding: utf-8 -*-
'''
    Calculadora con fines educativos para Tercero basico
    Enseñar Matematicas con la programacion
        Funciones
        * Multiplicacion (Como suma): Muestra la representacion,
            operacion y resultado de una multiplicacion
            ** Ejecucion
            python multiplicacion_como_suma.py multiplicando multiplicador
'''

import sys

# Inicializamos el flag de validacion
FLAG = None

# Validamos que los parametros sean digitos
if sys.argv[1].isdigit() == False or sys.argv[2].isdigit() == False:
    print "Los parametros de Multiplicando y Multiplicador no son digitos, \
            imposible ejecutar"
    exit()

# Si los parametros son digitos asignamos a variables
# Recibimos el primer parametro como el multiplicando
MULTIPLICANDO = int(sys.argv[1])
# Recibimos el primer parametro como el multiplicador
MULTIPLICADOR = int(sys.argv[2])

# Validamos que los parametros correspondan a las definiciones que queremos
if len(str(MULTIPLICANDO)) > 9 or MULTIPLICANDO < 0:
    print "MULTIPLICANDO debe ser un numero positivo, \
            y no tener un tamaño mayor a 10 digitos"
    FLAG = 1

if len(str(MULTIPLICANDO)) > 1 and (MULTIPLICADOR > 9 or MULTIPLICADOR < 1):
    print "MULTIPLICADOR debe ser menor a 10 y mayor que 0"
    FLAG = 1
# Si FLAG no esta vacio entonces hay un error y salimos
# El error es que no se cumplen nuestras definiciones
if FLAG:
    exit()

# Imprimimos la representacion de la multiplicacion
REPRESENTACION = 'Operacion: [' + str(MULTIPLICANDO) \
                    + ' x ' + str(MULTIPLICADOR) + ']'

print '+----'
print '| ' + REPRESENTACION
print '+----'

# Inicializamos las variables para la operacion
SUMA = 0
CICLO = 0

# Ejecutamos la operacion de la Calculadora
for iteraccion in range(0, MULTIPLICADOR):
    SUMA += MULTIPLICANDO
    CICLO += 1

    # Si el ciclo es igual a 1 imprimimos con este formato
    # la representacion
    if CICLO == 1:
        print '*: [%s x %s] = %s + 0 = %s' % (MULTIPLICANDO,
                                              CICLO, MULTIPLICANDO, SUMA)
        continue

    # Para los demas ciclos iremos concatenando la
    # representacion de la suma en una cadena de texto
    suma_como_cadena = '*: [%s x %s] = ' % (MULTIPLICANDO, CICLO)
    suma_como_cadena += (str(MULTIPLICANDO) + ' + ') * CICLO

    # Elimino el ultimo simbolo concatenado
    suma_a_imprimir = suma_como_cadena.rstrip(' + ')
    # Concateno el resultado para la representacion del producto
    suma_a_imprimir += ' = ' + str(SUMA)
    # Luego solo Imprimimos la cadena
    print suma_a_imprimir
print '+----'
